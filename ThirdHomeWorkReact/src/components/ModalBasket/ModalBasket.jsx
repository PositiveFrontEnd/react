import Modal from "../Modal/Modal";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalClose from "../Modal/ModalClose";
import "../Modal/Modal.scss";
import PropTypes from "prop-types";
import ModalHeader from "../Modal/ModalHeader";
const ModalBasket = ({ data, handleClose, isOpen, handleOK }) => {
  const { name, url } = data;
  return (
    <ModalWrapper isOpen={isOpen} clickWrapper={handleClose}>
      <Modal>
        <ModalClose firstColor click={handleClose} />
        <ModalHeader>
          <img className="box__img" src={url} alt="picture" />
        </ModalHeader>
        <ModalBody>
          <div className="box__info">
            <h4 className="box__name">Product Name : {name} </h4>
          </div>
        </ModalBody>
        <ModalFooter
          buttonClassNameFirst="button__white"
          secondBox
          textFirst="Delete ?"
          clickFirst={handleOK}
        />
      </Modal>
    </ModalWrapper>
  );
};
ModalBasket.propTypes = {
  handleClose: PropTypes.func,
  isOpen: PropTypes.bool,
  data: PropTypes.object,
  handleOK: PropTypes.func,
};
export default ModalBasket;
