import PropTypes from "prop-types";
import ShopNow from "../ShopNow/ShopNow";
import CardList from "./Card/CardsList";
const Main = ({
  handleBaskets,
  clickedIndexes,
  handleButtonClick,
  isAddedBaskets,
}) => {
  return (
    <section className="main">
      <ShopNow />
      <CardList
        isAddedBaskets={isAddedBaskets}
        handleButtonClick={handleButtonClick}
        handleBaskets={handleBaskets}
        clickedIndexes={clickedIndexes}
      />
    </section>
  );
};
Main.propTypes = {
  handleBaskets: PropTypes.func,
  clickedIndexes: PropTypes.array,
  handleButtonClick: PropTypes.func,
  isAddedBaskets: PropTypes.array,
};
export default Main;
