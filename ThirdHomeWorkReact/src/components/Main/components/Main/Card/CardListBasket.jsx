import CardBoxBasket from "./CardBox/CardBoxBasket";
import PropTypes from "prop-types";
import ModalBasket from "../../../../ModalBasket/ModalBasket";
import { useState } from "react";
import NotPage from "../../../../../Pages/NotPage/NotePage";
const CardListBasket = ({
  handelDeleteItemFromBaskets,
  basketsFromStorage,
  clickedIndexes,
  handleButtonClick,
}) => {
  const [isModalText, setModalText] = useState(false);
  const [currentCard, setCurrentCard] = useState({});
  const handelShowModal = () => setModalText(!isModalText);
  const handleCurrentCard = (cardPost) => setCurrentCard(cardPost);

  if (basketsFromStorage.length === 0) {
    return <NotPage />;
  } else {
    return (
      <>
        <CardBoxBasket
          handelShowModal={handelShowModal}
          cards={basketsFromStorage}
          handleCurrentCard={handleCurrentCard}
          clickedIndexes={clickedIndexes}
          handleButtonClick={handleButtonClick}
        />
        <ModalBasket
          data={currentCard}
          isOpen={isModalText}
          handleClose={handelShowModal}
          handleOK={() => {
            handelShowModal();
            handelDeleteItemFromBaskets(currentCard, currentCard.article);
          }}
        ></ModalBasket>
      </>
    );
  }
};
CardListBasket.propTypes = {
  clickedIndexes: PropTypes.array,
  handleButtonClick: PropTypes.func,
  handelDeleteItemFromBaskets: PropTypes.func,
  basketsFromStorage: PropTypes.array,
};

export default CardListBasket;
