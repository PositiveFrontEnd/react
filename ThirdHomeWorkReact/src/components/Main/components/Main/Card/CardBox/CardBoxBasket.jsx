import PropTypes from "prop-types";
import BoxPicture from "./BoxPicture";
import BoxInfo from "./BoxInfo";
import Heart from "../../../../../Header/Search/icons/heart.svg?react";
import "./CardBox.scss";
import Button from "../../../../../Button/Button";
const CardBoxBasket = ({
  cards,
  handelShowModal,
  handleCurrentCard,
  clickedIndexes,
  handleButtonClick,
}) => {
  const card = cards.map((item) => (
    <div className="card__box box " key={item.article}>
      <BoxPicture data={item} /> <BoxInfo data={item} />
      <div className="card__button__box">
        <Button
          className="card__button"
          type="button"
          click={() => {
            handelShowModal();
            handleCurrentCard(item, item.article);
          }}
        >
          {"Delete"}
        </Button>
        <Button
          className={`card__button-heart ${
            clickedIndexes.includes(item.article)
              ? "card__button-heart-red"
              : ""
          }`}
          click={() => {
            handleCurrentCard(item);
            handleButtonClick(item, item.article);
          }}
        >
          {<Heart />}
        </Button>
      </div>
    </div>
  ));
  return <div className="card">{card}</div>;
};

CardBoxBasket.propTypes = {
  cards: PropTypes.array,
  handleButtonClick: PropTypes.func,
  handelShowModal: PropTypes.func,
  handleCurrentCard: PropTypes.func,
  clickedIndexes: PropTypes.array,
};

export default CardBoxBasket;
