import CardBox from "./CardBox/CardBox";
import PropTypes from "prop-types";
import ModalText from "../../../../ModalText/ModalText";
import { useState } from "react";
import NotPage from "../../../../../Pages/NotPage/NotePage";
const CardListFavorite = ({
  clickedIndexes,
  favoritesFromStorage,
  handleBaskets,
  handleButtonClick,
  isAddedBaskets,
}) => {
  const [isModalText, setModalText] = useState(false);
  const [currentCard, setCurrentCard] = useState({});
  const handelShowModal = () => setModalText(!isModalText);
  const handleCurrentCard = (cardPost) => setCurrentCard(cardPost);

  if (favoritesFromStorage.length === 0) {
    return <NotPage />;
  } else {
    return (
      <>
        <CardBox
          isAddedBaskets={isAddedBaskets}
          handelShowModal={handelShowModal}
          cards={favoritesFromStorage}
          handleCurrentCard={handleCurrentCard}
          clickedIndexes={clickedIndexes}
          handleButtonClick={handleButtonClick}
        />
        <ModalText
          data={currentCard}
          isOpen={isModalText}
          handleClose={handelShowModal}
          handleOK={() => {
            handelShowModal();
            handleBaskets(currentCard, currentCard.article);
          }}
        ></ModalText>
      </>
    );
  }
};
CardListFavorite.propTypes = {
  handleBaskets: PropTypes.func,
  clickedIndexes: PropTypes.array,
  handleButtonClick: PropTypes.func,
  isAddedBaskets: PropTypes.array,
  favoritesFromStorage: PropTypes.array,
};

export default CardListFavorite;
