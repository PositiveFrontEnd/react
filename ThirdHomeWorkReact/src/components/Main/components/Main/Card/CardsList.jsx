import CardBox from "./CardBox/CardBox";
import PropTypes from "prop-types";
import ModalText from "../../../../ModalText/ModalText";
import { sendRequest } from "../../../../Helpers/SendRequest";
import { useEffect, useState } from "react";
const CardList = ({
  handleBaskets,
  clickedIndexes,
  handleButtonClick,
  isAddedBaskets,
}) => {
  const [cards, setCards] = useState([]);
  const [isModalText, setModalText] = useState(false);
  const [currentCard, setCurrentCard] = useState({});
  const handelShowModal = () => setModalText(!isModalText);
  const handleCurrentCard = (cardPost) => setCurrentCard(cardPost);

  useEffect(() => {
    sendRequest("./Cards.json").then((result) => {
      setCards(result);
    });
  }, []);
  return (
    <>
      <CardBox
        isAddedBaskets={isAddedBaskets}
        handelShowModal={handelShowModal}
        cards={cards}
        handleCurrentCard={handleCurrentCard}
        clickedIndexes={clickedIndexes}
        handleButtonClick={handleButtonClick}
      />
      <ModalText
        data={currentCard}
        isOpen={isModalText}
        handleClose={handelShowModal}
        handleOK={() => {
          handelShowModal();
          handleBaskets(currentCard, currentCard.article);
        }}
      ></ModalText>
    </>
  );
};
CardList.propTypes = {
  handleBaskets: PropTypes.func,
  clickedIndexes: PropTypes.array,
  handleButtonClick: PropTypes.func,
  isAddedBaskets: PropTypes.array,
};

export default CardList;
