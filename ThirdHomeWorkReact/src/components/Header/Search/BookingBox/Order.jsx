import { Link } from "react-router-dom";
import Button from "../../../Button/Button";
import Orders from "../icons/basket.svg?react";
const Order = ({ baskets }) => {
  if (baskets.length === 0) {
    return (
      <span>
        <Link to="/BasketPage">
          <Button className="booking__button">
            <Orders />
          </Button>
        </Link>
      </span>
    );
  } else
    return (
      <span>
        {baskets.length}
        <Link to="/BasketPage">
          <Button className="booking__button">
            <Orders />
          </Button>
        </Link>
      </span>
    );
};

export default Order;
