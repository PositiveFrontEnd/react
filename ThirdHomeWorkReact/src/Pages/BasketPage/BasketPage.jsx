import CardListBasket from "../../components/Main/components/Main/Card/CardListBasket";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import PropTypes from "prop-types";
const BasketPage = ({
  handleButtonClick,
  basketsFromStorage,
  favorites,
  baskets,
  handelDeleteItemFromBaskets,
  clickedIndexes,
}) => {
  return (
    <>
      <Header favorites={favorites} baskets={baskets} />
      <CardListBasket
        clickedIndexes={clickedIndexes}
        handelDeleteItemFromBaskets={handelDeleteItemFromBaskets}
        basketsFromStorage={basketsFromStorage}
        handleButtonClick={handleButtonClick}
      />
      <Footer />
    </>
  );
};
BasketPage.propTypes = {
  handleButtonClick: PropTypes.func,
  basketsFromStorage: PropTypes.array,
  favorites: PropTypes.array,
  baskets: PropTypes.array,
  handelDeleteItemFromBaskets: PropTypes.func,
  clickedIndexes: PropTypes.array,
};
export default BasketPage;
