import CardListFavorite from "../../components/Main/components/Main/Card/CardListFavorite";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import PropTypes from "prop-types";
const FavoritePage = ({
  clickedIndexes,
  favoritesFromStorage,
  handleBaskets,
  handleButtonClick,
  favorites,
  baskets,
  isAddedBaskets,
}) => {
  return (
    <>
      <Header favorites={favorites} baskets={baskets} />
      <CardListFavorite
        clickedIndexes={clickedIndexes}
        favoritesFromStorage={favoritesFromStorage}
        handleBaskets={handleBaskets}
        handleButtonClick={handleButtonClick}
        isAddedBaskets={isAddedBaskets}
      />
      <Footer />
    </>
  );
};
FavoritePage.propTYpes = {
  clickedIndexes: PropTypes.array,
  favoritesFromStorage: PropTypes.array,
  handleBaskets: PropTypes.func,
  handleButtonClick: PropTypes.func,
  favorites: PropTypes.array,
  baskets: PropTypes.array,
  isAddedBaskets: PropTypes.array,
};
export default FavoritePage;
