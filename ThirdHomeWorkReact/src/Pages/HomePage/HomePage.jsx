import Header from "../../components/Header/Header";
import Main from "../../components/Main/components/Main/Main";
import Footer from "../../components/Footer/Footer";
import PropTypes from "prop-types";
const HomePage = ({
  favorites,
  baskets,
  isAddedBaskets,
  handleBaskets,
  handleButtonClick,
  clickedIndexes,
}) => {
  return (
    <>
      <Header favorites={favorites} baskets={baskets} />
      <Main
        isAddedBaskets={isAddedBaskets}
        handleBaskets={handleBaskets}
        handleButtonClick={handleButtonClick}
        clickedIndexes={clickedIndexes}
      />
      <Footer />
    </>
  );
};
HomePage.propTypes = {
  favorites: PropTypes.array,
  baskets: PropTypes.array,
  isAddedBaskets: PropTypes.array,
  handleBaskets: PropTypes.func,
  handleButtonClick: PropTypes.func,
  clickedIndexes: PropTypes.array,
};
export default HomePage;
