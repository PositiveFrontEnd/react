import "./Reset.css";
import "./App.css";
import HomePage from "./Pages/HomePage/HomePage";
import { Route, Routes } from "react-router-dom";
import FavoritePage from "./Pages/FavoritePage/FavoritePage";
import BasketPage from "./Pages/BasketPage/BasketPage";
import NotPage from "./Pages/NotPage/NotePage";
import { useState, useEffect } from "react";
const App = () => {
  const favoritesFromStorage = JSON.parse(
    localStorage.getItem("favorites") || "[]"
  );
  const clickedIndexesFromStorage = JSON.parse(
    localStorage.getItem("clickedIndexes") || "[]"
  );
  const basketsFromStorage = JSON.parse(
    localStorage.getItem("baskets") || "[]"
  );

  const basketsItemFromStorage = JSON.parse(
    localStorage.getItem("basketsItem") || "[]"
  );
  const [favorites, setFavorites] = useState([...favoritesFromStorage]);
  const [baskets, setBaskets] = useState([...basketsFromStorage]);
  const [clickedIndexes, setClickedIndexes] = useState([
    ...clickedIndexesFromStorage,
  ]);
  const [basketsItem, setBasketsItem] = useState([...basketsItemFromStorage]);

  useEffect(() => {
    const handleLocalStorage = () => {
      localStorage.setItem("favorites", JSON.stringify(favorites));
      localStorage.setItem("clickedIndexes", JSON.stringify(clickedIndexes));
      localStorage.setItem("baskets", JSON.stringify(baskets));
      localStorage.setItem("basketsItem", JSON.stringify(basketsItem));
    };
    handleLocalStorage();
  }, [favorites, clickedIndexes, baskets, basketsItem]);

  const handleButtonClick = (item, article) => {
    const isAddedFavorite = favorites.some(
      (favorite) => favorite.article === item.article
    );

    if (isAddedFavorite) {
      const exitClickedIndex = clickedIndexes.filter(
        (added) => added !== article
      );
      setClickedIndexes(exitClickedIndex);
      const exitFavorite = favorites.filter(
        (favorite) => favorite.article !== item.article
      );
      setFavorites(exitFavorite);
    } else {
      setClickedIndexes((clickedIndexes) => [...clickedIndexes, article]);
      setFavorites((favorites) => [...favorites, item]);
    }
  };

  const handleBaskets = (item, article) => {
    const isAddedBaskets = baskets.some((basket) => {
      basket.article === item.article;
    });
    if (!isAddedBaskets) {
      setBasketsItem((basketsItem) => [...basketsItem, article]);
      setBaskets((prevBaskets) => [...prevBaskets, item]);
    }
  };

  const handelDeleteItemFromBaskets = (item, article) => {
    const isDeleteBaskets = baskets.some((basket) => {
      basket.article === item.article;
    });

    if (!isDeleteBaskets) {
      const isDeleteBasketsIndex = basketsItem.filter(
        (added) => added !== article
      );
      setBasketsItem(isDeleteBasketsIndex);
      const isDeleteBaskets = baskets.filter(
        (basket) => basket.article !== item.article
      );
      setBaskets(isDeleteBaskets);
    }
  };
  return (
    <>
      <Routes>
        <Route
          index
          element={
            <HomePage
              isAddedBaskets={basketsItem}
              handleBaskets={handleBaskets}
              handleButtonClick={handleButtonClick}
              clickedIndexes={clickedIndexes}
              favorites={favorites}
              baskets={baskets}
            />
          }
        />
        <Route
          path="/FavoritePage"
          element={
            <FavoritePage
              clickedIndexes={clickedIndexes}
              favoritesFromStorage={favorites}
              handleBaskets={handleBaskets}
              handleButtonClick={handleButtonClick}
              favorites={favorites}
              baskets={baskets}
              isAddedBaskets={basketsItem}
            />
          }
        />
        <Route
          path="/BasketPage"
          element={
            <BasketPage
              clickedIndexes={clickedIndexes}
              handelDeleteItemFromBaskets={handelDeleteItemFromBaskets}
              basketsFromStorage={baskets}
              handleButtonClick={handleButtonClick}
              favorites={favorites}
              baskets={baskets}
            />
          }
        />
        <Route path="*" element={<NotPage />} />
      </Routes>
    </>
  );
};

export default App;
