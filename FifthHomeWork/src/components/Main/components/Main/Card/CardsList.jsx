import React from "react";
import CardBox from "./CardBox/CardBox";
import ModalText from "../../../../ModalText/ModalText";
import {
  selectorBaskets,
  selectorCurrentCard,
  selectorIsModal,
} from "../../../../../store/selectors";
import { useSelector, useDispatch } from "react-redux";
import {
  actionIsModal,
  actionAddToBasket,
  actionsDeleteBasketCount,
} from "../../../../../store/actions";
const CardList = () => {
  const isModal = useSelector(selectorIsModal);
  const handelShowModal = () => dispatch(actionIsModal(!isModal));
  const baskets = useSelector(selectorBaskets);
  const currentCard = useSelector(selectorCurrentCard);
  const dispatch = useDispatch();

  const handleBaskets = (item, article) => {
    const isAddedBaskets = baskets.some((basket) => {
      return basket.article === item.article;
    });

    if (isAddedBaskets) {
      dispatch(actionsDeleteBasketCount(item));
    } else {
      dispatch(actionAddToBasket(currentCard, article));
    }
  };

  return (
    <>
      <CardBox handelShowModal={handelShowModal} />
      <ModalText
        data={currentCard}
        handleClose={handelShowModal}
        handleOK={() => {
          handelShowModal();
          handleBaskets(currentCard, currentCard.article);
        }}
      ></ModalText>
    </>
  );
};

export default CardList;
