import ModalText from "./components/ModalText/ModalText";
import ModalImage from "./components/ModalImage/ModalImage";
import { useState } from "react";
import BodyButton from "./components/Modal/BodyButton";
import "./App.css";
const App = () => {
  const [isModalImg, setModalImg] = useState(false);
  const [isModalText, setModalText] = useState(false);
  const handleClose = () => setModalImg(!isModalImg);
  const handleCancel = () => setModalText(!isModalText);
  return (
    <>
      <BodyButton
        showFirstModal={handleClose}
        showSecondModal={handleCancel}
      ></BodyButton>
      <ModalImage
        isOpen={isModalImg}
        title="Product Delete!"
        desc="By clicking the Yes , Delete button , PRODUCT NAME will be deleted."
        handleClose={handleClose}
        clickWrapper={handleClose}
      ></ModalImage>
      <ModalText
        isOpen={isModalText}
        title="Add Product NAME "
        desc="Description for you product"
        handleClose={handleCancel}
        clickWrapper={handleCancel}
      ></ModalText>
    </>
  );
};

export default App;
