import Modal from "../Modal/Modal";
import ModalBody from "../Modal/ModalBody";
import ModalHeader from "../Modal/ModalHeader";
import ModalFooter from "../Modal/ModalFooter";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalClose from "../Modal/ModalClose";
import "../Modal/Modal.scss";
import PropTypes from "prop-types";
const ModalImage = ({
  title,
  desc,
  handleCancel,
  handleClose,
  handleDelete,
  isOpen,
  clickWrapper,
}) => {
  return (
    <ModalWrapper isOpen={isOpen} clickWrapper={clickWrapper}>
      <Modal>
        <ModalClose firstColor click={handleClose} />
        <ModalHeader></ModalHeader>
        <ModalBody>
          <h2>{title}</h2>
          <p>{desc}</p>
        </ModalBody>
        <ModalFooter
          buttonClassNameFirst="button-purple"
          buttonClassNameSecond="button-white"
          firstBox
          textFirst="NO,CANCEL"
          textSecondary="YES,DELETE"
          clickFirst={handleCancel}
          clickSecondary={handleDelete}
        />
      </Modal>
    </ModalWrapper>
  );
};

ModalImage.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  handleCancel: PropTypes.func,
  handleClose: PropTypes.func,
  handleDelete: PropTypes.func,
  isOpen: PropTypes.bool,
  clickWrapper: PropTypes.func,
};
export default ModalImage;
