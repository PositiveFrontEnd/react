import Button from "../Button/Button";
import PropTypes from "prop-types";
import "./BodyButton";
const BodyButton = ({
  textFirst,
  textSecondary,
  showFirstModal,
  showSecondModal,
}) => {
  return (
    <>
      <Button firstModal click={showFirstModal}>
        {(textFirst = "Open first modal")}
      </Button>
      <Button secondModal click={showSecondModal}>
        {(textSecondary = "Open second modal")}
      </Button>
    </>
  );
};
Button.propTypes = {
  textFirst: PropTypes.string,
  textSecondary: PropTypes.string,
  showSecondModal: PropTypes.func,
  showFirstModal: PropTypes.func,
};
export default BodyButton;
