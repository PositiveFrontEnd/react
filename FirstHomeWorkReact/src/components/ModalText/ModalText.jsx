import Modal from "../Modal/Modal";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalClose from "../Modal/ModalClose";
import "../Modal/Modal.scss";
import PropTypes from "prop-types";
const ModalText = ({
  title,
  desc,
  handleCancel,
  handleClose,
  isOpen,
  clickWrapper,
}) => {
  return (
    <ModalWrapper isOpen={isOpen} clickWrapper={clickWrapper}>
      <Modal classNames="wrapper__box-second">
        <ModalClose secondColor click={handleClose} />
        <ModalBody>
          <h2>{title}</h2>
          <p>{desc}</p>
        </ModalBody>
        <ModalFooter
          buttonClassNameFirst="button-green"
          secondBox
          textFirst="ADD TO FAVORITE"
          clickFirst={handleCancel}
        />
      </Modal>
    </ModalWrapper>
  );
};
ModalText.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  handleCancel: PropTypes.func,
  handleClose: PropTypes.func,
  isOpen: PropTypes.bool,
  clickWrapper: PropTypes.func,
};
export default ModalText;
