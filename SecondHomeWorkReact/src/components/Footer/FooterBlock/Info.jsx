import PropTypes from "prop-types";
const Info = ({ text }) => {
  return (
    <a className="footer__info" href="#">
      {text}
    </a>
  );
};
Info.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
};
export default Info;
