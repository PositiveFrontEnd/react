import "./Header.scss";
import Nav from "./Nav/Nav";
import Search from "./Search/Search";
import PropTypes from "prop-types";
import Logo from "./Logo/Logo";
import BookingBox from "./Search/BookingBox/BookingBox";
const Header = ({ favorites, baskets }) => {
  return (
    <header className="header">
      <Logo />
      <Nav />
      <Search />
      <BookingBox favorites={favorites} baskets={baskets} />
    </header>
  );
};
Header.propTypes = {
  className: PropTypes.string,
  favorites: PropTypes.array,
  baskets: PropTypes.array,
};
export default Header;
