import NavList from "./NavList";
import ListItem from "./LIstItem";
import "./Nav.scss";
const Nav = () => {
  return (
    <div className="nav">
      <NavList>
        <ListItem text="Shop" />
        <ListItem text="Men" />
        <ListItem text="Women" />
        <ListItem text="Combos" />
        <ListItem text="Joggers" />
      </NavList>
    </div>
  );
};

export default Nav;
