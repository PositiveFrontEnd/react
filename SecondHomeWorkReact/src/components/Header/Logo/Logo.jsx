import PropTypes from "prop-types";
import Group from "./Group";
import Euphoria from "./Euphoria";
import "./Logo.scss";
const Logo = () => {
  return (
    <a href="#" className="nav__logo logo">
      <Euphoria></Euphoria>
      <Group></Group>
    </a>
  );
};

export default Logo;
