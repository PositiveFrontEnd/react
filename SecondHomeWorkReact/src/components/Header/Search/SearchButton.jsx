import PropTypes from "prop-types";
import Loop from "./icons/loop.svg?react";
const SearchButton = ({ className, type, click }) => {
  return (
    <button type={type} className={className} onClick={click}>
      <Loop />
    </button>
  );
};
SearchButton.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
};
export default SearchButton;
