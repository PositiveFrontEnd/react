import Favorite from "./Favorite";
import Order from "./Order";
import PropTypes from "prop-types";
import "./BookingBox.scss";
const BookingBox = ({ baskets, favorites }) => {
  return (
    <div className="booking">
      <Favorite favorites={favorites} />
      <Order baskets={baskets} />
    </div>
  );
};

BookingBox.propTypes = {
  className: PropTypes.string,
  favorites: PropTypes.array,
  baskets: PropTypes.array,
};
export default BookingBox;
