import Orders from "../icons/basket.svg?react";
const Order = ({ baskets }) => {
  if (baskets.length === 0) {
    return (
      <span>
        <Orders />
      </span>
    );
  } else
    return (
      <span>
        {baskets.length}
        <Orders />
      </span>
    );
};

export default Order;
