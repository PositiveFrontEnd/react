import Favorites from "../icons/heart.svg?react";
const Favorite = ({ favorites }) => {
  if (favorites.length === 0) {
    return (
      <span>
        <Favorites />
      </span>
    );
  } else
    return (
      <span>
        {favorites.length}
        <Favorites />
      </span>
    );
};
export default Favorite;
