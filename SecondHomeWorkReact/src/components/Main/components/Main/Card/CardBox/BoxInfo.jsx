import PropTypes from "prop-types";
const BoxInfo = ({ data }) => {
  const { price, article, color, name } = data;

  return (
    <div className="box__info">
      <h4 className="box__name">Product Name : {name}</h4>
      <p className="box__price">Price : {price} ₴ </p>
      <span className="box__article">Product Code : {article}</span>
      <p className="box__color">Product Color : {color}</p>
    </div>
  );
};
BoxInfo.propTypes = {
  data: PropTypes.object,
};
export default BoxInfo;
