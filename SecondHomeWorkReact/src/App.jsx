import "./Reset.css";
import Header from "./components/Header/Header";
import Main from "./components/Main/components/Main/Main";
import Footer from "./components/Footer/Footer";
import "./App.css";
import { useState, useEffect } from "react";

const App = () => {
  const favoritesFromStorage = JSON.parse(
    localStorage.getItem("favorites") || "[]"
  );
  const clickedIndexesFromStorage = JSON.parse(
    localStorage.getItem("clickedIndexes") || "[]"
  );
  const basketsFromStorage = JSON.parse(
    localStorage.getItem("baskets") || "[]"
  );
  const basketsItemFromStorage = JSON.parse(
    localStorage.getItem("basketsItem") || "[]"
  );
  const [favorites, setFavorites] = useState([...favoritesFromStorage]);
  const [baskets, setBaskets] = useState([...basketsFromStorage]);
  const [clickedIndexes, setClickedIndexes] = useState([
    ...clickedIndexesFromStorage,
  ]);
  const [basketsItem, setBasketsItem] = useState([...basketsItemFromStorage]);

  useEffect(() => {
    const handleLocalStorage = () => {
      localStorage.setItem("favorites", JSON.stringify(favorites));
      localStorage.setItem("clickedIndexes", JSON.stringify(clickedIndexes));
      localStorage.setItem("baskets", JSON.stringify(baskets));
      localStorage.setItem("basketsItem", JSON.stringify(basketsItem));
    };
    handleLocalStorage();
  }, [favorites, clickedIndexes, baskets, basketsItem]);

  const handleButtonClick = (item, article) => {
    const isAddedFavorite = favorites.some(
      (favorite) => favorite.article === item.article
    );

    if (isAddedFavorite) {
      const exitClickedIndex = clickedIndexes.filter(
        (added) => added !== article
      );
      setClickedIndexes(exitClickedIndex);
      const exitFavorite = favorites.filter(
        (favorite) => favorite.article !== item.article
      );
      setFavorites(exitFavorite);
    } else {
      setClickedIndexes((clickedIndexes) => [...clickedIndexes, article]);
      setFavorites((favorites) => [...favorites, item]);
    }
  };

  const handleBaskets = (item, article) => {
    const isAddedBaskets = baskets.some((basket) => {
      basket.article === item.article;
    });

    if (isAddedBaskets) {
      const isAddedBaskets = basketsItem.filter((added) => added !== article);
      setBasketsItem(isAddedBaskets);
    } else {
      setBasketsItem((basketsItem) => [...basketsItem, article]);
      setBaskets((prevBaskets) => [...prevBaskets, item]);
    }
  };

  return (
    <>
      <Header favorites={favorites} baskets={baskets} />
      <Main
        isAddedBaskets={basketsItem}
        handleBaskets={handleBaskets}
        handleButtonClick={handleButtonClick}
        clickedIndexes={clickedIndexes}
      />
      <Footer />
    </>
  );
};

export default App;
