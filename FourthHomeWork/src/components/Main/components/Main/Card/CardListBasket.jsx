import CardBoxBasket from "./CardBox/CardBoxBasket";
import ModalBasket from "../../../../ModalBasket/ModalBasket";
import NotPage from "../../../../../Pages/NotPage/NotePage";
import { useSelector, useDispatch } from "react-redux";
import {
  selectorBaskets,
  selectorIsModal,
  selectorCurrentCard,
} from "../../../../../store/selectors";
import {
  actionDeleteItemFromBasket,
  actionIsModal,
} from "../../../../../store/actions";
const CardListBasket = () => {
  const isModal = useSelector(selectorIsModal);
  const handelShowModal = () => dispatch(actionIsModal(!isModal));

  const currentCard = useSelector(selectorCurrentCard);
  const baskets = useSelector(selectorBaskets);
  const dispatch = useDispatch();
  const handelDeleteItemFromBaskets = (item, article) => {
    dispatch(actionDeleteItemFromBasket(item, article));
  };

  if (baskets.length === 0) {
    return <NotPage />;
  } else {
    return (
      <>
        <CardBoxBasket handelShowModal={handelShowModal} />
        <ModalBasket
          data={currentCard}
          handleClose={handelShowModal}
          handleOK={() => {
            handelShowModal();
            handelDeleteItemFromBaskets(currentCard, currentCard.article);
          }}
        ></ModalBasket>
      </>
    );
  }
};

export default CardListBasket;
