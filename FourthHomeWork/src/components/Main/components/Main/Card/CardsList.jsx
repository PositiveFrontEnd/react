import CardBox from "./CardBox/CardBox";
import ModalText from "../../../../ModalText/ModalText";
import {
  selectorCurrentCard,
  selectorIsModal,
} from "../../../../../store/selectors";
import { useSelector, useDispatch } from "react-redux";
import { actionIsModal, actionAddToBasket } from "../../../../../store/actions";
const CardList = () => {
  const isModal = useSelector(selectorIsModal);
  const handelShowModal = () => dispatch(actionIsModal(!isModal));

  const currentCard = useSelector(selectorCurrentCard);
  const dispatch = useDispatch();
  const handleBaskets = (item, article) => {
    dispatch(actionAddToBasket(item, article));
  };

  return (
    <>
      <CardBox handelShowModal={handelShowModal} />
      <ModalText
        data={currentCard}
        handleClose={handelShowModal}
        handleOK={() => {
          handelShowModal();
          handleBaskets(currentCard, currentCard.article);
        }}
      ></ModalText>
    </>
  );
};

export default CardList;
