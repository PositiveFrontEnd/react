import CardBoxFavorite from "./CardBox/CardBoxFavorite.jsx";
import ModalText from "../../../../ModalText/ModalText";
import NotPage from "../../../../../Pages/NotPage/NotePage";
import { useSelector, useDispatch } from "react-redux";
import {
  selectorCurrentCard,
  selectorIsModal,
  selectFavorites,
} from "../../../../../store/selectors";
import {
  actionIsModal,
  actionAddToBasket,
} from "../../../../../store/actions.js";
const CardListFavorite = () => {
  const isModal = useSelector(selectorIsModal);
  const handelShowModal = () => dispatch(actionIsModal(!isModal));
  const currentCard = useSelector(selectorCurrentCard);
  const dispatch = useDispatch();
  const handleBaskets = (item, article) => {
    dispatch(actionAddToBasket(item, article));
  };
  const favorites = useSelector(selectFavorites);
  if (favorites.length === 0) {
    return <NotPage />;
  } else {
    return (
      <>
        <CardBoxFavorite handelShowModal={handelShowModal} />
        <ModalText
          data={currentCard}
          handleClose={handelShowModal}
          handleOK={() => {
            handelShowModal();
            handleBaskets(currentCard, currentCard.article);
          }}
        ></ModalText>
      </>
    );
  }
};

export default CardListFavorite;
